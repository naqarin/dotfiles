Collection of dotfiles, based on dotbot.

## Requirements

* zsh
* oh-my-zsh
* vim
* hyper
* nerd-fonts
* tmux
* visual-studio-code
* node
* npm
